import 'reflect-metadata';
import express from "express";
import cors from "cors";
import helmet from "helmet";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";

import { StateDb } from './connect-db';

import { JWTHelper } from "./helpers/jwt.helper";

import { RouteDefinition } from "./models/dicts.model";
import { AuthController } from "./controllers/auth.controller";
import { CompanyController } from "./controllers/company.controller";
import { EmployeeController } from "./controllers/employee.controller";


const app = express();
const db = new StateDb();

const jwtHelper = new JWTHelper();

/**
 *  App Configuration
 */
app.use(helmet());
app.use(cors());
app.use(express.json());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }));
app.use(cookieParser());

// Iterate over all our controllers and register our routes
[
    AuthController,
    CompanyController,
    EmployeeController
].forEach(controller => {
    // This is our instantiated class
    const instance: any = new controller();
    // The prefix saved to our controller
    const prefix = Reflect.getMetadata('prefix', controller);
    // Our `routes` array containing all our routes for this controller
    const routes: Array<RouteDefinition> = Reflect.getMetadata('routes', controller);

    // Iterate over all routes and register them to our express application 
    routes.forEach(route => {
        app[route.requestMethod](prefix + route.path, route.validators, route.isCheckAuthorization ? jwtHelper.verifyToken : jwtHelper.notForbidden, (req: express.Request, res: express.Response) => {
            // Execute our method for this path and pass our express request and response object.
            instance[route.methodName!](req, res).then((data: any) => {
                res.status(200).json(data);
            });
        });
    });
});

// TODO: I know this is trash
async function initial(){
     setTimeout(_=>{
         db.get().collection('users').find({}).toArray().then((data)=>{
            if(data.length == 0){
                db.get().collection('users').insertOne({
                    email: 'test@mail.ru',
                    password: '$2a$10$SEoh73lHxd5asg8N3aj5SOnd.F66sSzILNN2rLHyBG2tFtbBaf9lK',
                    lastName: 'Test',
                    firstName: 'Test',
                    isActivated: true,
                    role: 'admin'
                });
            }
        })
    }, 400);
}

export function App() {

    initial();

    return app;
}