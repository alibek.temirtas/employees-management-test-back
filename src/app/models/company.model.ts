import { DbEntityModel } from './db-entity.model';

export class Company extends DbEntityModel {
    readonly _id?: string;
    name: string;
    description: string;

    static CollectionName: string = 'companies';

    constructor(obj: {
        name: string,
        description: string
    }) {
        super();
        this.name = obj.name;
        this.description = obj.description;
    };
}

export interface IModifiedCompany {
    readonly _id?: string;
    name: string;
    description: string;
}