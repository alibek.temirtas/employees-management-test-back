import { DbEntityModel } from './db-entity.model';
import {IModifiedCompany} from '../models/company.model';
/*
    TODO: Separate user on admin and employee role
          Currently admin and employee store in one collection (users)
*/

export class User extends DbEntityModel {
    readonly _id?: string;
    firstName: string;
    lastName: string;
    middleName: string;
    email: string;
    password: string;
    
    companyId: string;
    isActivated: boolean;
    role: 'admin' | 'employee';

    static CollectionName: string = 'users';

    constructor(obj: {
        firstName: string,
        lastName: string,
        middleName: string,
        password: string,
        email: string,
        companyId: string,
        isActivated: boolean,
        role: 'admin' | 'employee'
    }) {
        super();
        this.firstName = obj.firstName;
        this.lastName = obj.lastName;
        this.middleName = obj.middleName;
        this.password = obj.password;
        this.email = obj.email;
        this.companyId = obj.companyId;
        this.isActivated = obj.isActivated || false;
        this.role = obj.role || 'admin';
    };
}

export interface IModifiedUser {
    readonly _id?: string;
    firstName: string;
    lastName: string;
    middleName: string;
    email: string;
    role: 'admin' | 'employee';
    company: IModifiedCompany | null;
}
