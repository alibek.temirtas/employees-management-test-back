import moment from "moment";

export class DbEntityModel {
    createdDate: number | null;
    updatedDate: number | null;
    deletedDate: number | null;
    isDeleted: boolean;

    constructor() {
        this.createdDate = moment.now();
        this.updatedDate = null;
        this.deletedDate = null;
        this.isDeleted = false;
    }
}

