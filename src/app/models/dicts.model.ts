export interface RouteDefinition {
  path: string;
  requestMethod: 'get' | 'post' | 'delete' | 'options' | 'put';
  isCheckAuthorization?: boolean;
  validators?: any;
  // Method name within our class responsible for this route
  methodName?: string;
}

export interface IResponseDefinition<T> {
  success: boolean;
  data: T | null;
  errors: any;
}

export interface IResponseWithPagination<T> {
  success: boolean;
  data: {
    items: Array<T>;
    pageIndex: number;
    pageSize: number;
    totalCount: number;
  };
  errors: any;
}

export interface IResponseDefinitionAuth {
  userId: string;
  message: string;
}

export interface IResponseDefinitionLogin {
  accessToken?: string;
  message: string;
}

export interface IPagerQuery {
  perPage: number;
  page: number;
}