import { MongoClient, Db } from 'mongodb';

const DB_NAME: string = process.env.DB_NAME!;

let state: { db: Db | null } = {
    db: null
}

export class MongoDBConnection {

    constructor() { };

    connect(url: string, done: Function) {
        if (state.db) {
            return done();
        }

        MongoClient.connect(url, { useNewUrlParser: true}, (error, database) => {
            if (error) {
                return console.log(error);
            }
            state.db = database.db(DB_NAME);
            return done();
        });
    }
}

export class StateDb {
    constructor() { };

    get(): Db {
        return state.db!;
    }
}
