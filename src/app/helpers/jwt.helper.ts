import { NextFunction } from "express";
import jwt from "jsonwebtoken";

export class JWTHelper {
    encode(payload: any) {
        return jwt.sign({
            data: payload
          }, process.env.SECRET_KEY!, { expiresIn: 60 * 6000 })
    }

    decoded(_jwt: string) {
        return jwt.verify(_jwt, process.env.SECRET_KEY!, (error, res) => {
          if (error) {
            throw error;
          }
          return res;
        });
      }

      verifyToken(req: any, res: any, next: NextFunction) {
        const token = req.headers['authorization'] ? req.headers['authorization'].split(' ')[1] : null;
        if (!token) return res.status(403).send({auth: false, msg: 'No token provided.'});
      

        jwt.verify(token, process.env.SECRET_KEY!, (err: any, decoded: any) => {

          if (err) return res.status(403).send({auth: false, msg: 'Failed to authenticate token.'});
      
          req.userId = decoded.data._id;
          next();
        });
      }

      notForbidden(req: any, res: any, next: NextFunction){
        next();
      }
      
}