
import { StateDb } from '../connect-db';
import { IPagerQuery } from '../models/dicts.model';

const db = new StateDb();

export class DbContextHelper<T> {

    async insertOne(collectionName: string, doc: T): Promise<any> {
        return db.get().collection(collectionName).insertOne(doc);
    }

    async findOne(collectionName: string, query: any = {}): Promise<any> {
        return db.get().collection(collectionName).findOne(query);
    }

    async find(collectionName: string, query: any = {}, pager: IPagerQuery): Promise<any> {
        return db.get().collection(collectionName).find(query).skip((pager.perPage * pager.page) - pager.perPage).limit(pager.perPage).sort({ _id: -1 }).toArray();
    }

    async getCount(collectionName: string, query: any = {}) {
        return db.get().collection(collectionName).find(query).count();
    }


    async updateOne(collectionName: string, query: any, update: any): Promise<any> {
        return db.get().collection(collectionName).updateOne(query, update);
    }

    async deleteOne(collectionName: string, query: any): Promise<any> {
        return db.get().collection(collectionName).deleteOne(query);
    }

}
