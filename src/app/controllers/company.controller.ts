import express from "express";
import { ObjectId } from "mongodb";
import { check, validationResult } from "express-validator";

import { Controller } from "../helpers/decorators/controller.decorator";
import { ApiRequest } from "../helpers/decorators/route.decorator";

import { DbContextHelper } from "../helpers/db-context.helper";
import { IResponseDefinition, IResponseWithPagination } from "../models/dicts.model";
import { Company, IModifiedCompany } from "../models/company.model";

import {CompanyDao} from "../dao/company.dao";
import {CompanyDto} from "../dto/company.dto";

const companyDao = new CompanyDao(new DbContextHelper<Company>());
const companyDto = new CompanyDto();

@Controller('/company')
export class CompanyController {

    /*
                * @api {post} /company/
                * @apiGroup company
                * @apiParam {string} name company's name
                * @apiParam {string} description
                * 
                * 
                * @apiParamExample {json} body
                *    {
                *      "name": "GitHub",
                *      "description": "Repository company"
                *    }
                * @apiSuccess {String} data Product
                * @apiSuccessExample {json} Success
                *      HTTP/1.1 200 OK
                *      {
                *          "success": true,
                *          "data": {
                *                       "_id": "5e9c73691f04f61f6f34dad9",
                *                        "name": "GitHub",
                *                        "description": "Repository company",
                *               },
                *           "errors": []
                *       }
        */
       @ApiRequest({
        path: '/',
        requestMethod: 'post',
        validators: [
            check('name', 'Company name is required').not().isEmpty()
        ],
        isCheckAuthorization: true
    })
    async createCompany(req: express.Request, res: express.Response, next: express.NextFunction): Promise<IResponseDefinition<IModifiedCompany>> {

        let result: IResponseDefinition<IModifiedCompany> = {
            success: false,
            data: null,
            errors: []
        };

        try {
            const errors: any = validationResult(req);

            if (!errors.isEmpty()) {
                throw errors.errors;
            }

            let companyDoc = new Company(req.body);

            const getCompanyResult = await companyDao.getCompany({ name: companyDoc.name });

            if(getCompanyResult){
                throw 'Company has been already created with that name';
            }

            const createCompanyResult = await companyDao.createCompany(companyDoc);

            if (!createCompanyResult.insertedId) {
                throw 'Error inserting data';
            }

            result = {
                success: true,
                data: {
                    _id: createCompanyResult.insertedId,
                    ...companyDto.getModifiedData(companyDoc)
                },
                errors: []
            };

        } catch (error) {
            result = {
                success: false,
                data: null,
                errors: error
            }
        }

        return result;
    };

    /*
                    * @api {get} /company/?page=1&perpage=20 
                    * @apiGroup company
                    * @apiParam {string | number} page
                    * @apiParam {string | number} perpage
                    *
                    * @apiSuccess {String} data Company[]
                    * @apiSuccessExample {json} Success
                    *      HTTP/1.1 200 OK
                    *      {
                    *          "success": true,
                    *          "data": {
                    *               items:
                    *                     [{
                    *                     "_id": "5ea15edbfb63b1260c3b28de",
                    *                        "name": "GitHub",
                    *                        "description": "Repository company",
                    *                  }],
                    *                pageIndex: 1,
                    *                pageSize: 20,
                    *                totalCount: 1
                    *               },
                    *           "errors": []
                    *       }
            */
        @ApiRequest({
            path: '/',
            requestMethod: 'get',
            validators: [],
            isCheckAuthorization: true
        })
        async getCompanyList(req: express.Request, res: express.Response, next: express.NextFunction): Promise<IResponseWithPagination<IModifiedCompany> | IResponseDefinition<null>> {
    
            let result: IResponseDefinition<null> | IResponseWithPagination<IModifiedCompany> = {
                success: false,
                data: null,
                errors: []
            };
    
            const PER_PAGE = 10;
            const PAGE = 1;
    
            const page = +req.query.page || PAGE,
                  perpage = +req.query.perpage || PER_PAGE;
    
            try {
    
                const companyListResult = await companyDao.getCompanysList({}, {
                    page: page,
                    perPage: perpage
                });
    
                if (!companyListResult) {
                    throw 'Adertisements not found';
                }
    
                const totalCount = await companyDao.getTotalCount({});
    
                result = {
                    success: true,
                    data: {
                        items: companyDto.getModifiedDataList(companyListResult),
                        pageIndex: page,
                        pageSize: perpage,
                        totalCount
                    },
                    errors: []
                };
    
            } catch (error) {
                result = {
                    success: false,
                    data: null,
                    errors: error
                }
            }
    
            return result;
        };

    /*
    * @api {put} /company/
    */
   @ApiRequest({
        path: '/',
        requestMethod: 'put',
        validators: [
            check('companyId', 'companyId is required id').not().isEmpty(),
            check('name', 'Name is required').not().isEmpty()
        ],
        isCheckAuthorization: true
    })
    async updateCompany(req: express.Request, res: express.Response, next: express.NextFunction): Promise<IResponseDefinition<IModifiedCompany>> {

        let result: IResponseDefinition<IModifiedCompany> = {
            success: false,
            data: null,
            errors: []
        };

        try {
            const errors: any = validationResult(req);

            if (!errors.isEmpty()) {
                throw errors.errors;
            }

            const { companyId, name } = req.body;

            if(await companyDao.getCompany({ name: name })){
                throw 'Can not update with this name';
            }

            const company = await companyDao.getCompany({ _id: new ObjectId(companyId) });

            if (!company) {
                throw 'Company not found';
            }

            const updatedResult = await companyDao.updateCompany({ _id: new ObjectId(companyId) }, req.body);

            if (!updatedResult.result.ok) {
                throw 'Error updating data';
            }

            result = {
                success: true,
                data: {
                    ...req.body
                },
                errors: []
            };

        } catch (error) {
            result = {
                success: false,
                data: null,
                errors: error
            }
        }

        return result;
    };

    /*
    * @api {delete} /company/

    * TODO : create validation of employees
             if company has employees reject removing.

    */
   @ApiRequest({
        path: '/',
        requestMethod: 'delete',
        validators: [
            check('companyId', 'companyId is required').not().isEmpty()
        ],
        isCheckAuthorization: true
    })
    async deleteCompany(req: express.Request, res: express.Response, next: express.NextFunction): Promise<IResponseDefinition<any>> {

        let result: IResponseDefinition<any> = {
            success: false,
            data: null,
            errors: []
        };

        try {
            const errors: any = validationResult(req);

            if (!errors.isEmpty()) {
                throw errors.errors;
            }

            const { companyId } = req.query;

            const company = await companyDao.getCompany({ _id: new ObjectId(companyId) });

            if (!company) {
                throw 'Company not found';
            }

            const deletedResult = await companyDao.deleteCompany({ _id: new ObjectId(companyId) });

            if (!deletedResult.result.ok) {
                throw 'Error delete company';
            }

            result = {
                success: true,
                data: {
                    companyId: companyId
                },
                errors: []
            };

        } catch (error) {
            result = {
                success: false,
                data: null,
                errors: error
            }
        }

        return result;
    };
}