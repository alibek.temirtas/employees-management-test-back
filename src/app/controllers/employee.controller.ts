import express from "express";
import { ObjectId } from "mongodb";
import bcrypt from "bcryptjs";
import { check, validationResult, Result } from "express-validator";

import { Controller } from "../helpers/decorators/controller.decorator";
import { ApiRequest } from "../helpers/decorators/route.decorator";
import { DbContextHelper } from "../helpers/db-context.helper";
import { User } from "../models/user.model";
import { IResponseDefinition, IResponseWithPagination } from "../models/dicts.model";


import { UserDto } from "../dto/user.dto";
import { UserDao } from "../dao/user.dao";

const userDao = new UserDao(new DbContextHelper<User>());
const userDto = new UserDto();

const hashPassword = async (password: string) => bcrypt.hash(password, 10);

@Controller('/employee')
export class EmployeeController {

    /*
        * @api {post} /employee
        * @apiGroup employee
        * @apiParamExample {json} body
        *    {
        *      "firstName": "Alibek",
        *      "lastName": "Temirtas",
        *      "middleName": "GalymbekUly",
        *      "email": "alibek.temirtas@mail.ru",
        *      "companyId": "1",
        *      "password": "123456"
        *    }
    */
       @ApiRequest({
        path: '/',
        requestMethod: 'post',
        validators: [
            check('firstName', 'First name is required').not().isEmpty(),
            check('lastName', 'Last name is required').not().isEmpty(),
            check('companyId', 'companyId is required').not().isEmpty(),
            check('email', 'Email is required').not().isEmpty(),
            check('email', 'Incorrect email address').isEmail(),
            check('password', 'Password is required').not().isEmpty(),
            check('password', 'Length of password should be minimum 6').isLength({ min: 6 })
        ]
    })
    async createEmployee(req: express.Request, res: express.Response, next: express.NextFunction): Promise<IResponseDefinition<{userId: string, message: string}>> {
        let result: IResponseDefinition<{userId: string, message: string}> = {
            success: false,
            data: null,
            errors: []
        };

        try {
            const errors: Result | any = validationResult(req);

            if (!errors.isEmpty()) {
                throw errors.errors;
            }

            let userDoc = new User({
                ...req.body,
                isActivated: true,
                role: 'employee',
                password: await hashPassword(req.body.password)
            });

            const getUserResult = await userDao.getUser({ email: userDoc.email });

            if (getUserResult) {
                throw 'Employee already created';
            } else {
                const addUserResult = await userDao.addUser(userDoc);

                if (!addUserResult.insertedId) {
                    throw 'Error inserting data';
                }

                result = {
                    success: true,
                    data: {
                        userId: addUserResult.insertedId,
                        message: 'Employee successfully created'
                    },
                    errors: []
                }
                
            }

        } catch (error) {
            result = {
                success: false,
                data: null,
                errors: error
            }
        }

        return result;
    }

    /*
        * @api {get} /employee/list/?page=1&perpage=20 
    */
        @ApiRequest({
            path: '/list',
            requestMethod: 'get',
            validators: [],
            isCheckAuthorization: true
        })
        async getEmployeeList(req: express.Request, res: express.Response, next: express.NextFunction): Promise<IResponseWithPagination<any> | IResponseDefinition<null>> {
    
            let result: IResponseDefinition<null> | IResponseWithPagination<any> = {
                success: false,
                data: null,
                errors: []
            };
    
            const PER_PAGE = 10;
            const PAGE = 1;

    
            const page = +req.query.page || PAGE,
                  perpage = +req.query.perpage || PER_PAGE,
                  companyId = req.query.companyId;            
            
            let query = {};

            if(companyId){
                   query = {
                        companyId
                    } 
            }
            try {
    
                const listResult = await userDao.getUsersList({...query, role:'employee'}, {
                    page: page,
                    perPage: perpage
                });
    
                if (!listResult) {
                    throw 'Users not found';
                }
    
                const totalCount = await userDao.getTotalCount({});

                const users = await userDto.getModifiedDataList(listResult);
    
                result = {
                    success: true,
                    data: {
                        items: users,
                        pageIndex: page,
                        pageSize: perpage,
                        totalCount
                    },
                    errors: []
                };
    
            } catch (error) {
                result = {
                    success: false,
                    data: null,
                    errors: error
                }
            }
    
            return result;
        };

    
    /*
    * @api {put} /employee/
    */
   @ApiRequest({
        path: '/',
        requestMethod: 'put',
        validators: [
            check('userId', 'userId is required id').not().isEmpty(),
            check('firstName', 'First name is required').not().isEmpty(),
            check('lastName', 'Last name is required').not().isEmpty(),
            check('companyId', 'companyId is required').not().isEmpty()
        ]
    })
    async updateUser(req: express.Request, res: express.Response, next: express.NextFunction): Promise<IResponseDefinition<any>> {

        let result: IResponseDefinition<any> = {
            success: false,
            data: null,
            errors: []
        };

        try {
            const errors: any = validationResult(req);

            if (!errors.isEmpty()) {
                throw errors.errors;
            }

            const { userId } = req.body;

            const user = await userDao.getUser({ _id: new ObjectId(userId) });

            if (!user) {
                throw 'user not found';
            }

            const updatedResult = await userDao.updateUser({ _id: new ObjectId(userId) }, req.body);

            if (!updatedResult.result.ok) {
                throw 'Error updating data';
            }

            result = {
                success: true,
                data: {
                    ...req.body
                },
                errors: []
            };

        } catch (error) {
            result = {
                success: false,
                data: null,
                errors: error
            }
        }

        return result;
    };

    /*
    * @api {delete} /employee/

    */
   @ApiRequest({
        path: '/',
        requestMethod: 'delete',
        validators: [
            check('userId', 'userId is required').not().isEmpty()
        ]
    })
    async deleteUser(req: express.Request, res: express.Response, next: express.NextFunction): Promise<IResponseDefinition<any>> {

        let result: IResponseDefinition<any> = {
            success: false,
            data: null,
            errors: []
        };

        try {
            const errors: any = validationResult(req);

            if (!errors.isEmpty()) {
                throw errors.errors;
            }

            const { userId } = req.query;

            const user = await userDao.getUser({ _id: new ObjectId(userId) });

            if (!user) {
                throw 'User not found';
            }

            const deletedResult = await userDao.deleteUser({ _id: new ObjectId(userId) });

            if (!deletedResult.result.ok) {
                throw 'Error delete user';
            }

            result = {
                success: true,
                data: {
                    userId: userId
                },
                errors: []
            };

        } catch (error) {
            result = {
                success: false,
                data: null,
                errors: error
            }
        }

        return result;
    };
  
}