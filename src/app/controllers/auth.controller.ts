import express from "express";
import { ObjectId } from "mongodb";
import bcrypt from "bcryptjs";
import { check, validationResult, Result } from "express-validator";

import { Controller } from "../helpers/decorators/controller.decorator";
import { ApiRequest } from "../helpers/decorators/route.decorator";
import { DbContextHelper } from "../helpers/db-context.helper";
import { User } from "../models/user.model";
import { IResponseDefinition, IResponseDefinitionAuth, IResponseDefinitionLogin } from "../models/dicts.model";

import { JWTHelper } from "../helpers/jwt.helper";
import { UserDao } from "../dao/user.dao";
import { UserDto } from "../dto/user.dto";

const userDao = new UserDao(new DbContextHelper<User>());
const userDto = new UserDto();
const jwtHelper = new JWTHelper();

const hashPassword = async (password: string) => bcrypt.hash(password, 10);

/*
    TODO : Sdelat udalenie polzovatelya po isDeleted: true, i postavit proverki po etomu property gde vezde.
           Sdelat proverku na nalichie company id
*/

@Controller('/auth')
export class AuthController {
    /*
        * @api {post} /auth/register Register a new user
        * @apiGroup Users
        * @apiParam {String} firstName User first name
        * @apiParam {String} lastName User last name
        * @apiParam {String} middleName User middle name
        * @apiParam {String} companyId Choosen company
        * @apiParam {String} email User's email
        * @apiParam {String} password User password
        * @apiParamExample {json} body
        *    {
        *      "firstName": "Alibek",
        *      "lastName": "Temirtas",
        *      "middleName": "GalymbekUly",
        *      "email": "alibek.temirtas@mail.ru",
        *      "companyId": "1",
        *      "password": "123456"
        *    }
        * @apiSuccess {Boolean} success User registration status
        * @apiSuccess {String} message Message description
        * @apiSuccess {String} userId User's id that was created
        * @apiSuccessExample {json} Success
        *      HTTP/1.1 200 OK
        *      {
        *          "success": true,
        *          "data": {
        *               userId: '452lf1o2mdo',
        *               message: 'You have registered'
        *           },
        *           "errors": []
        *       }
    */
    @ApiRequest({
        path: '/register',
        requestMethod: 'post',
        validators: [
            check('firstName', 'First name is required').not().isEmpty(),
            check('lastName', 'Last name is required').not().isEmpty(),
            check('companyId', 'companyId is required').not().isEmpty(),
            check('email', 'Email is required').not().isEmpty(),
            check('email', 'Incorrect email address').isEmail(),
            check('password', 'Password is required').not().isEmpty(),
            check('password', 'Length of password should be minimum 6').isLength({ min: 6 })
        ]
    })
    async register(req: express.Request, res: express.Response, next: express.NextFunction): Promise<IResponseDefinition<IResponseDefinitionAuth>> {
        let result: IResponseDefinition<IResponseDefinitionAuth> = {
            success: false,
            data: null,
            errors: []
        };

        try {
            const errors: Result | any = validationResult(req);

            if (!errors.isEmpty()) {
                throw errors.errors;
            }

            let userDoc = new User({
                ...req.body,
                password: await hashPassword(req.body.password)
            });

            const getUserResult = await userDao.getUser({ email: userDoc.email });

            if (getUserResult) {
                if (getUserResult.isActivated) {
                    throw 'User already registered';
                } else {
                    result = {
                        success: true,
                        data: {
                            userId: getUserResult._id!,
                            message: 'Wait permissions from administrator.'
                        },
                        errors: []
                    }
                }
            } else {
                const addUserResult = await userDao.addUser(userDoc);

                if (!addUserResult.insertedId) {
                    throw 'Error inserting data';
                }

                result = {
                    success: true,
                    data: {
                        userId: addUserResult.insertedId,
                        message: 'You have registered. Please, wait permissions from administrator.'
                    },
                    errors: []
                }
                
            }

        } catch (error) {
            result = {
                success: false,
                data: null,
                errors: error
            }
        }

        return result;
    }

    /*
            * @api {post} /auth/activateuser Activation user 
            * @apiGroup Users
            * @apiParam {String} userId User id
            * @apiParamExample {json} body
            *    {
            *      "userId":"5o123mijn323odmo12e"
            *    }
            * @apiSuccess {Boolean} success Activation user status
            * @apiSuccess {String} message Message description
            * @apiSuccessExample {json} Success
            *      HTTP/1.1 200 OK
            *      {
            *          "success": true,
            *          "data": {
            *               message: 'User has been activated'
            *           },
            *           "errors": []
            *       }
    */
    @ApiRequest({
        path: '/activateuser',
        requestMethod: 'post',
        validators: [
            check('userId', 'User id should be not a empty').not().isEmpty()
        ]
    })
    async activateUser(req: express.Request, res: express.Response, next: express.NextFunction): Promise<IResponseDefinition<any>> {


        let result: IResponseDefinition<any> = {
            success: false,
            data: null,
            errors: []
        };

        try {
            const errors: Result | any = validationResult(req);

            if (!errors.isEmpty()) {
                throw errors.errors;
            }

            let payloadDoc: {
                userId: string
            } = req.body;

            const getUserResult = await userDao.getUser({ _id: new ObjectId(payloadDoc.userId) });

            if (!getUserResult) {
                throw 'User not found';
            }

            if (getUserResult.isActivated) {
                throw 'User has been already activated';
            }

            const updateUserResult = await userDao.activateUser({ _id: new ObjectId(payloadDoc.userId) });

            if (!updateUserResult.result.nModified) {
                throw 'Error mongodb activation user';
            }

            result = {
                success: true,
                data: {
                    message: 'User has been activated'
                },
                errors: []
            }

        } catch (error) {
            result = {
                success: false,
                data: null,
                errors: error
            }
        }

        return result;

    }

    /*
            * @api {post} /auth/login 
            * @apiGroup Users
            * @apiParam {Number} email User's email
            * @apiParam {String} password User's password
            * @apiParamExample {json} body
            *    {
            *      "email":"alibek.temirtas@mail.ru",
            *      "password": "123456"
            *    }
            * @apiSuccess {Boolean} success Authorization user status
            * @apiSuccess {String} accessToken authorization token
            * @apiSuccess {String} message Message description
            * @apiSuccessExample {json} Success
            *      HTTP/1.1 200 OK
            *      {
            *          "success": true,
            *          "data": {
            *               accessToken: "521awdwerf1d12d23d23"
            *               message: 'Authorization has been successfully'
            *           },
            *           "errors": []
            *       }
    */
    @ApiRequest({
        path: '/login',
        requestMethod: 'post',
        validators: [
            check('password', 'Password is required').not().isEmpty(),
            check('email', 'Email is required').not().isEmpty(),
            check('email', 'Incorrect email address').isEmail()
        ]
    })
    async login(req: express.Request, res: express.Response, next: express.NextFunction): Promise<IResponseDefinition<IResponseDefinitionLogin>> {

        let result: IResponseDefinition<IResponseDefinitionLogin> = {
            success: false,
            data: null,
            errors: []
        };

        try {
            const errors: Result | any  = validationResult(req);
            if (!errors.isEmpty()) {
                throw errors.errors;
            }

            let payloadDoc: {
                email: string,
                password: string
            } = req.body;

            const getUserResult = await userDao.getUser({ email: payloadDoc.email });

            if (!getUserResult) {
                throw 'User not found';
            }

            if (!getUserResult.isActivated) {
                throw 'User was not activated';
            }

            const passwordIsValid: boolean = bcrypt.compareSync(payloadDoc.password.toString(), getUserResult.password);

            if (!passwordIsValid) {
                throw 'Password is not valid';
            }

            const accessToken = jwtHelper.encode(getUserResult);


            result = {
                success: true,
                data: {
                    accessToken,
                    message: 'Authorization has been successfully'
                },
                errors: []
            }

        } catch (error) {
            result = {
                success: false,
                data: null,
                errors: error
            }
        }
        return result;
    }

    /*
        * @api {get} /auth/user/info
    */
   @ApiRequest({
    path: '/user/info',
    requestMethod: 'get',
    validators: [],
    isCheckAuthorization: true
})
async getUserInfo(req: any, res: express.Response, next: express.NextFunction): Promise<any> {

    let result: IResponseDefinition<any> = {
        success: false,
        data: null,
        errors: []
    };

    const {userId} = req;

    try {

        const userResult = await userDao.getUser({_id: new ObjectId(userId)});

        if (!userResult) {
            throw 'Users not found';
        }

        // const user = await userDto.getModifiedData(userResult);

        result = {
            success: true,
            data: {
                user: userResult
            },
            errors: []
        };

    } catch (error) {
        result = {
            success: false,
            data: null,
            errors: error
        }
    }

    return result;
};
}
