import { InsertOneWriteOpResult, UpdateWriteOpResult } from "mongodb";
import { DbContextHelper } from "../helpers/db-context.helper";
import { User } from "../models/user.model";
import { IPagerQuery } from '../models/dicts.model';

export class UserDao {
    dbContextHelper: DbContextHelper<User>;

    constructor(_dbContextHelper: DbContextHelper<User>) {
        this.dbContextHelper = _dbContextHelper;
    }

    async addUser(user: User): Promise<InsertOneWriteOpResult<any>> {
        return await this.dbContextHelper.insertOne(User.CollectionName, user);
    }

    async getUser(query: any = {}): Promise<User | null> {
        return await this.dbContextHelper.findOne(User.CollectionName, query);
    }

    async getUsersList(query: any = {}, pager: IPagerQuery): Promise<User[] | null> {
        return await this.dbContextHelper.find(User.CollectionName, {
            isDeleted: false,
            ...query
        }, pager);
    }

    async activateUser(query: any): Promise<UpdateWriteOpResult> {
        return await this.dbContextHelper.updateOne(User.CollectionName, query, {
            $set: {
                isActivated: true
            }
        });
    }

    async deleteUser(query: any): Promise<UpdateWriteOpResult> {
        return await this.dbContextHelper.deleteOne(User.CollectionName, query);
    }

    async updateUser(query: any, doc: any): Promise<UpdateWriteOpResult> {
        return await this.dbContextHelper.updateOne(User.CollectionName, query, {
            $set: {
                ...doc
            }
        });
    }

    async getTotalCount(query: any = {}): Promise<any> {
        return await this.dbContextHelper.getCount(User.CollectionName, {
            isDeleted: false,
            ...query
        });
    }

    async changePass(query: any, newPassword: string): Promise<UpdateWriteOpResult> {
        return await this.dbContextHelper.updateOne(User.CollectionName, query, {
            $set: {
                password: newPassword
            }
        });
    }
}