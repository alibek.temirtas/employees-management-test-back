import { InsertOneWriteOpResult, UpdateWriteOpResult } from "mongodb";
import { DbContextHelper } from "../helpers/db-context.helper";
import { Company } from "../models/company.model";
import { IPagerQuery } from '../models/dicts.model';

export class CompanyDao {
    dbContextHelper: DbContextHelper<Company>;

    constructor(_dbContextHelper: DbContextHelper<Company>) {
        this.dbContextHelper = _dbContextHelper;
    }

    async createCompany(company: Company): Promise<InsertOneWriteOpResult<any>> {
        return await this.dbContextHelper.insertOne(Company.CollectionName, company);
    }

    async getCompany(query: any = {}): Promise<Company | null> {
        return await this.dbContextHelper.findOne(Company.CollectionName, query);
    }

    async getCompanysList(query: any = {}, pager: IPagerQuery): Promise<Company[] | null> {
        return await this.dbContextHelper.find(Company.CollectionName, {
            isDeleted: false,
            ...query
        }, pager);
    }

    async getTotalCount(query: any = {}): Promise<any> {
        return await this.dbContextHelper.getCount(Company.CollectionName, {
            isDeleted: false,
            ...query
        });
    }

    async deleteCompany(query: any): Promise<UpdateWriteOpResult> {
        return await this.dbContextHelper.deleteOne(Company.CollectionName, query);
    }

    async updateCompany(query: any, doc: any): Promise<UpdateWriteOpResult> {
        return await this.dbContextHelper.updateOne(Company.CollectionName, query, {
            $set: {
                ...doc
            }
        });
    }
}