import { User } from "../models/user.model";
import { Company } from "../models/company.model";
import { ObjectId } from "mongodb";

import { DbContextHelper } from "../helpers/db-context.helper";

import { CompanyDto } from "../dto/company.dto";
import { CompanyDao } from "../dao/company.dao";

const companyDao = new CompanyDao(new DbContextHelper<Company>());
const companyDto = new CompanyDto();
 
export class UserDto {

    constructor() {
    }

    async getModifiedData(user: User) {
        return await this._modifyUser(user);
    }

    async getModifiedDataList(users: User[]) {
        return this._modifyUsers(users)!;
    }

    private async _modifyUsers(_users: User[]) {
        return Promise.all(_users.map((user) => { 
            return this._modifyUser(user).then((result) => { 
                return result;
            });
        }));
    }    

    private async _modifyUser(user: User) {
        
        const company = await companyDao.getCompany({ _id: new ObjectId(user.companyId) }) || null;

        return {
            _id: user._id,
            firstName: user.firstName,
            lastName: user.lastName,
            middleName: user.middleName,
            email: user.email,
            role: user.role,
            company: companyDto.getModifiedData(company)
        }
    }
}