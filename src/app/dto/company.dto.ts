import { Company, IModifiedCompany } from "../models/company.model";

export class CompanyDto {

    constructor() {
    }

    getModifiedData(company: Company | null): IModifiedCompany {
        return this._modifyCompany(company);
    }

    getModifiedDataList(companies: Company[]): IModifiedCompany[] {
        return this._modifyCompanies(companies)!;
    }

    private _modifyCompanies(_companies: Company[]): IModifiedCompany[] {
        return _companies.map((company)=>{
            return this._modifyCompany(company);
        });
    }

    private _modifyCompany(company: Company | any): IModifiedCompany {
        return {
            _id: company._id,
            name: company.name,
            description: company.description
        }
    }
}