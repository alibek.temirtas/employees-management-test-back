/**
 * 
 * RUN MONGO IN DOCKER 
 * 
 * sudo docker run --rm -d -p 27017:27017 mongo
 * into docker: sudo docker exec -it <container name> /bin/bash
 * 
 */
import * as dotenv from "dotenv";
import http from "http";
import { App } from "./app/app";
import { MongoDBConnection } from "./app/connect-db";

dotenv.config();

const app = App();
const mongoDbConnection = new MongoDBConnection();

/**
 * App Variables
 */
if (!process.env.PORT) {
    process.exit(1);
}

const PORT: number = parseInt(process.env.PORT as string, 10);
const DB_URL: string = process.env.DB_URL!;
const DB_PORT: string = process.env.DB_PORT!;
const DB_NAME: string = process.env.DB_NAME!;


/**
 * Server & MongoDB Activation
 */

const server = http.createServer(app);

mongoDbConnection.connect(`mongodb://${DB_URL}:${DB_PORT}/${DB_NAME}`, (err: any) => {
    if (err) {
        console.log('Error db connection: ', err);
        return;
    }

    server.listen(PORT, () => {
        console.log(`Listening on port: ${PORT}`);
    });
});


/**
 * Webpack HMR Activation
 */

type ModuleId = string | number;

interface WebpackHotModule {
    hot?: {
        data: any;
        accept(
            dependencies: string[],
            callback?: (updatedDependencies: ModuleId[]) => void,
        ): void;
        accept(dependency: string, callback?: () => void): void;
        accept(errHandler?: (err: Error) => void): void;
        dispose(callback: (data: any) => void): void;
    };
}

declare const module: WebpackHotModule;

if (module.hot) {
    module.hot.accept();
    module.hot.dispose(() => server.close());
}