const webpack = require("webpack");
const path = require("path");
const nodeExternals = require("webpack-node-externals");
const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const WebpackShellPlugin = require('webpack-shell-plugin');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

const NODE_ENV = process.env.NODE_ENV;
let config = {
  plugins: [
    new CleanWebpackPlugin(),
    new webpack.HotModuleReplacementPlugin()
  ]
}

if (NODE_ENV !== 'production') {
  console.log('We are in development mode!');
  config.plugins.push(new WebpackShellPlugin({onBuildEnd: ['nodemon dist/index.min.js --watch dist']}));
}

module.exports = {
  entry: ["webpack/hot/poll?100", "./src/index.ts"],
  devtool: NODE_ENV === 'development' ? 'inline-source-map' : false,
  watch: NODE_ENV === 'development',
  target: "node",
  externals: [
    nodeExternals({
      whitelist: ["webpack/hot/poll?100"]
    })
  ],
  module: {
    rules: [
      {
        test: /.tsx?$/,
        use: "ts-loader",
        exclude: /node_modules/
      }
    ]
  },
  mode: NODE_ENV,
  resolve: {
    extensions: [".tsx", ".ts", ".js"]
  },
  plugins: [
    ...config.plugins
  ],
  output: {
    path: path.join(__dirname, "dist"),
    filename: "index.min.js"
  },
  optimization: {
    minimize: NODE_ENV === 'development' ? false : true,
    minimizer: [new UglifyJsPlugin({
      include: /\.min\.js$/
    })]
  }
};
